<?php


namespace App\Classes\Happily;

class Amilon
{
  // Amilon username
  private $username = "HAPPILYWS";

  // Amilon password
  private $password = "eG126xQ11";

  // Amilon contract code
  private $contractCode = "760cf3fe-611e-42f1-93d4-e778ce6fd09d";
  
  private $baseUrlStagingWS = "http://staging.b2bsales.amilon.eu";
  
  private $baseUrlProdWS = "https://b2bsales.amilon.eu";
  
  private $ws_name = "/WS/OrderService.asmx";

  // URL to USE. Change this for using staging or prod
  private $urlWS = '';

  // Amilon currency guid. You can't change it
  private $currencyCode = "3f2504e0-4f89-11d3-9a0c-0305e82c1111";

  private $is_test = true;
  
  

  /**
   *  Constructor
   *
   * 
   */
  public function __construct(  )
  {
    $this->mylog_message( __FUNCTION__, 'istest: ' . $this->is_test, 'INFO');
    
    // carica ambiente test o produzione in base a valore variabile
    if( true == $this->is_test )
    {
      $this->urlWS = $this->baseUrlStagingWS . $this->ws_name;
    }
    else
    {
      $this->urlWS = $this->baseUrlProdWS . $this->ws_name;
    }
  }


  /**
   * Get URL WS
   */
  public function get_base_url_WS()
  {
    if( true == $this->is_test )
    {
      return $this->baseUrlStagingWS;
    }
    else
    {
      return $this->baseUrlProdWS;
    }
  }
  
  
  /**
   * Set Amilon credentials
   * 
   * @param $username: Amilon username provided by Amilon
   * @param $password: Amilon password provided by Amilon
   * @param $contractCode: Amilon contract code provided by Amilon
   */
  public function setCredentials( $username, $password, $contractCode )
  {
    $this->username = $username;
    $this->password = $password;
    $this->contractCode = $contractCode;
  }



  /**
   * Create a new Amilon order
   * 
   * Create XML structure, contact Amilon AWS for operation 'CreateOrder', 
   * then retrievea the result of teh operation.
   * The XML rsult is converted in array and filtered for providing only
   * essentials infos
   * 
   * @param $orderData: array that contains the information about the order
   * The structure is ( fields marked with * must be provided by the caller ):   
   *            
   * orderData['username']
   * orderData['password']
   * orderData['contractCode']
   *  
   * orderData['orderData']['Order']['ExternalOrderCode']
   * orderData['orderData']['Order']['CurrencyCode']
   * orderData['orderData']['Order']['ShippingAddress']
   * orderData['orderData']['Order']['GrossAmount']
   * orderData['orderData']['Order']['TotalRequestedCodes']
   * 
   * orderData['orderData']['OrderRows']['OrderRowData']['ExternalOrderCode']
   * orderData['orderData']['OrderRows']['OrderRowData']['ProductCode']*
   * orderData['orderData']['OrderRows']['OrderRowData']['Name']*
   * orderData['orderData']['OrderRows']['OrderRowData']['Surname']*
   * orderData['orderData']['OrderRows']['OrderRowData']['Email']*
   * orderData['orderData']['OrderRows']['OrderRowData']['Dedication']
   * orderData['orderData']['OrderRows']['OrderRowData']['OrderFrom']
   * orderData['orderData']['OrderRows']['OrderRowData']['OrderTo']
   * orderData['orderData']['OrderRows']['OrderRowData']['PriceForEach']*
   * orderData['orderData']['OrderRows']['OrderRowData']['Quantity']
   * 
   * @return Info about order :
   * $result['OPERATIONRESPONSE'] = OK or KO
   * $result['OPERATIONRESULT'] = Created order or error message
   * $result['VOUCHERS'] = array with all vouchers created
   * Voucher is formed by:
   * ["VoucherLink"]=> string(36) 
   * ["ValidityStarDate"]=> string  
   * ["ValidityEndDate"]=>  string 
   * ["ProductCode"]=> 
   * ["CardCode"]=> string
   * ["RetailerCode"]=> string 
   * ["RetailerName"]=>string
   * ["Name"]=>string
   * ["Surname"]=>  string 
   * ["Email"]=>  string 
   * ["OrderFrom"]=>string
   * ["OrderTo"]=>string
   * ["Amount"]=>string
   * 
   * @throw RuntimeException 
   */
  public function create_order( $orderData )
  {
    // operazione del WS da chiamare
    $operation = "CreateOrder";

    // Aggiunta dei dati necessari per fare la richiesta
    $orderData['username'] = $this->username;
    $orderData['password'] = $this->password;      
    $orderData['contractCode'] = $this->contractCode;

    $orderData['orderData']['Order']['CurrencyCode'] = $this->currencyCode;
    $orderData['orderData']['Order']['ShippingAddress'] = '';
    $orderData['orderData']['Order']['TotalRequestedCodes'] = 1;

    $orderData['orderData']['OrderRows']['OrderRowData']['ExternalOrderCode'] = 
      $orderData['orderData']['Order']['ExternalOrderCode'];   

    $orderData['orderData']['OrderRows']['OrderRowData']['Dedication'] = '';
    $orderData['orderData']['OrderRows']['OrderRowData']['OrderFrom'] = '';
    $orderData['orderData']['OrderRows']['OrderRowData']['OrderTo'] = '';

    $orderData['orderData']['OrderRows']['OrderRowData']['PriceForEach'] =
        floatval( $orderData['orderData']['OrderRows']['OrderRowData']['PriceForEach'] );

    $orderData['orderData']['OrderRows']['OrderRowData']['Quantity'] = 1;

    // crea struttura XML per fare richiesta
    $xml = $this->create_xml( 'CreateOrder', $orderData );
    
    // effettua richiesta
    $result = $this->php_http_post_xml( $this->urlWS, $xml->asXML() );

    // dalla risposta in formato XML costruisce un array
    $result_semplified = $this->create_create_order_result( $result );
    
    // l'array è il risultato finale
    return $result_semplified;
  }
  
  
    /**
   * Create the array result for createOrder operation
   *
   * @param xmlstr  XML contenente la risposta del WS
   *
   * @return An array containing the details about response
   *
   * $result['OPERATIONRESPONSE'] = OK or KO
   * $result['OPERATIONRESULT'] = Created order or error message
   * $result['VOUCHERS'] = array with all vouchers created
   * Voucher is formed by:
   * ["VoucherLink"]=> string(36) 
   * ["ValidityStarDate"]=> string  
   * ["ValidityEndDate"]=>  string 
   * ["ProductCode"]=> 
   * ["CardCode"]=> string
   * ["RetailerCode"]=> string 
   * ["RetailerName"]=>string
   * ["Name"]=>string
   * ["Surname"]=>  string 
   * ["Email"]=>  string 
   * ["OrderFrom"]=>string
   * ["OrderTo"]=>string
   * ["Amount"]=>string
   *
   */
  private function create_create_order_result( $xmlstr )
  {
    $result = array();
    $xmlParser = new \SimpleXMLElement( $xmlstr );
    
    // indispensabile per xpath, altrimenti i ns non vanno
    $xmlParser->registerXPathNamespace( 'c', 'http://tempuri.org/' );
    
      
    // TODO. aggiungere tutti i nodi da eliminare
    $nodesToRemove = $xmlParser->xpath("//c:Dedication  
                                                  "); 
    
    // usa DOM per eliminare i nodi non necessari
    if( !empty ( $nodesToRemove ) )
    {
      foreach( $nodesToRemove as $n)
      {
        $domRef = dom_import_simplexml($n);
        $domRef->parentNode->removeChild($domRef);
      }
    }
    
    // cerca tutte le info riguardanti i voucher
    $result['VOUCHERS'] = $xmlParser->xpath('//c:Voucher');
    
    // info sull'esito della richiesta
    $result['OPERATIONRESULT'] = 
      (string)( $xmlParser->xpath( '//c:OperationResult' )[0] ) ; 
    
    /* aggiunge il campo OPERATIONRESPONSE per indicare se OK o KO
        OPERATIONRESULT invece fornisce maggiori dettagli 
    */
    $this->add_operation_response( $result, 'Order created' );
    
    return $result;
  }
  
  
  
  /**
   * Get all products enabled for Contract and filtered by category
   * 
   * The products returned are visible and active
   * 
   * @param $categoryType: category for filtering
   *        0 = SHOPPING,
   *        1 = BUONI BENZINA
   *        2 = VIAGGI
   *        3 = RIVISTE E TV
   * 
   * @return Associative array with all products enabled for Contract and 
   *         filtered by category
   * 
   * $result['OPERATIONRESPONSE'] = ok or error message
   * $result['PRODUCTS'] = array with all products filtered by category
   * Product is formed by:
   * ["ProductCode"]=> string(36) 
   * ["Name"]=> string  
   * ["ShortDescription"]=>  string 
   * ["LongDescription"]=> 
   * ["Price"]=> string(8) 
   * ["Currency"]=> string 
   * ["ProductType"]=>string(7) 
   * ["MerchantCode"]=>string(36) 
   * ["MerchantName"]=>  string 
   * ["MerchantImageUrl"]=>  string 
   * ["MerchantImage100x50"]=>string(29) 
   * ["MerchantImage150x150"]=>string(42)
   * ["MerchantImage180x70"]=> string(41)
   * ["MerchantExtraShortDescription"]=>  string
   * ["MerchantShortDescription"]=> string
   * ["MerchantLongDescription"]=>  string 
   * ["MerchantTermsAndConditions"]=> string 
   * ["MerchantWebsite"]=> string(26) 
   * ["MerchantFacebookFanPage"]=>string
   * ["MerchantCategory1"]=> string(36) 
   * ["MerchantCategory2"]=>string(36) 
   * ["MerchantStoreLocatorUrl"]=>string(181) 
   * ["ImageUrl"]=>string(63)
   * ["Image136x86"]=> object(SimpleXMLElement)#31 (0) {}
   * ["Image461x292"]=> object(SimpleXMLElement)#30 (0) {}
   * ["Image200x200"]=> object(SimpleXMLElement)#4 (0) {}
   * ["Image300x190"]=> string(33)
   * ["Image560x292"]=> object(SimpleXMLElement)#33 (0) {}
   * ["Active"]=>  string(5)
   * ["Visible"]=> string(4)
   * ["ProductAvailability"]=> string(4)
   */
  public function get_products_for_contract( $categoryType )
  {
    $requestData['username'] = $this->username;
    $requestData['password'] = $this->password;      
    $requestData['contractCode'] = $this->contractCode;
    $requestData['includeAvailabilityInformation'] = true;
    
    // crea xml
    $xml = $this->create_xml( 'GetProductsForContractComplete', 
                              $requestData );

    // effettua richiesta
    $result = $this->php_http_post_xml( $this->urlWS, $xml->asXML() );

    // dalla risposta in formato XML costruisce un array
    $result_semplified = $this->create_get_products_result( $result, 
                                                            $categoryType);
    // l'array è il risultato
    return $result_semplified;
  }


  /**
   * Create the array result for getProductsForContract operation
   *
   * @param xmlstr  XML contenente la risposta del WS
   *
   * @return An array containing
   *
   * $result['OPERATIONRESPONSE'] = ok or error message
   * $result['PRODUCTS'] = array with all products filtered by category
   * Product is formed by:
   * ["ProductCode"]=> string(36) 
   * ["Name"]=> string  
   * ["ShortDescription"]=>  string 
   * ["LongDescription"]=> 
   * ["Price"]=> string(8) 
   * ["Currency"]=> string 
   * ["ProductType"]=>string(7) 
   * ["MerchantCode"]=>string(36) 
   * ["MerchantName"]=>  string 
   * ["MerchantImageUrl"]=>  string 
   * ["MerchantImage100x50"]=>string(29) 
   * ["MerchantImage150x150"]=>string(42)
   * ["MerchantImage180x70"]=> string(41)
   * ["MerchantExtraShortDescription"]=>  string
   * ["MerchantShortDescription"]=> string
   * ["MerchantLongDescription"]=>  string 
   * ["MerchantTermsAndConditions"]=> string 
   * ["MerchantWebsite"]=> string(26) 
   * ["MerchantFacebookFanPage"]=>string
   * ["MerchantCategory1"]=> string(36) 
   * ["MerchantCategory2"]=>string(36) 
   * ["MerchantStoreLocatorUrl"]=>string(181) 
   * ["ImageUrl"]=>string(63)
   * ["Image136x86"]=> object(SimpleXMLElement)#31 (0) {}
   * ["Image461x292"]=> object(SimpleXMLElement)#30 (0) {}
   * ["Image200x200"]=> object(SimpleXMLElement)#4 (0) {}
   * ["Image300x190"]=> string(33)
   * ["Image560x292"]=> object(SimpleXMLElement)#33 (0) {}
   * ["Active"]=>  string(5)
   * ["Visible"]=> string(4)
   * ["ProductAvailability"]=> string(4)
   *
   * TODO: Applicare filtri per categoria, visibilita e disponibilità
   *
   */
  private function create_get_products_result( $xmlstr, $categoryType )
  {
    $result = array();
    $xmlParser = new \SimpleXMLElement( $xmlstr );
    $xmlParser->registerXPathNamespace( 'c', 'http://tempuri.org/' );
    
    $result['OPERATIONRESPONSE'] = 
      (string)( $xmlParser->xpath( '//c:OperationResult' )[0] ) ; 
    
    /* Filtra la categoria del prodotto. vengono rimossi tutti i nodi che
      * non hanno un codice categoria relativo alla categoria passata come
      * parametro, e che non hanno un codice veditore relativo alla 
      * categoria passata come parametro.
      */      
    $catCodXpath = "";
    $merchCodXpath = "";
    $concOp = " and ";      
    if( 0 != $categoryType )
    {
      /* In base alla categoria vengono ritornati sia la lista dei codici
          merchant che sono dedicati ad una categoria, e la lista dei codici
          categoria che sono dedicati ad una categoria
      */
      $this->get_cat_codes_by_type( $categoryType, 
                                    $merchCodes, 
                                    $catCodes );

      /* siccome potrebbero esserci piu categorycode, costruisco la stringa da
          passare come parametro a xpath, in modo da poter eliminare tutti i
          nodi che non hanno quel valore nel categorycode1
        */        
      foreach( $catCodes as $c )
      { 
        $catCodXpath .= $concOp . "(not(text()='" . $c . "'))";                              
      } 
      
      /* siccome potrebbero esserci piu merchcode, costruisco la stringa da
          passare come parametro a xpath, in modo da poter eliminare tutti i
          nodi che non hanno quel valore nel merchcode
        */        
      foreach( $merchCodes as $c )
      { 
        $merchCodXpath .= $concOp . "(not(text()='" . $c . "'))";                              
      }         
          
      // se comincia con and lo rimuovo
      if( substr( $catCodXpath, 0, strlen( $concOp)) === $concOp )
      {
        $catCodXpath = substr( $catCodXpath, strlen( $concOp ) );
      }
      
      // se comincia con and lo rimuovo
      if( substr( $merchCodXpath, 0, strlen( $concOp )) === $concOp )
      {
        $merchCodXpath = substr( $merchCodXpath, strlen( $concOp ) );
      }
      
      // se vuoti o nulli li mette a USELESS per non far fallire xpath
      if( !isset( $catCodXpath ) || "" == $catCodXpath )
      {
        $catCodXpath = "(text() != 'USELESS')";
      }
      
      if( !isset( $merchCodXpath ) || "" == $merchCodXpath )
      {
        $merchCodXpath = "(text() != 'USELESS')";
      }
      
      // xpath: oltre alle categorie eliminare anche non visibili e i non attivi
      $totalXpath = "//c:Product[c:Visible[text()='false'] |
                                c:Active[text()='true'] |
                                (c:MerchantCategory1[" . $catCodXpath  . "]) and 
                                (c:MerchantCode[" . $merchCodXpath  . "])
                                ]";
    }
    else
    {
      /* se non c'è selzione categoria, toglieri dal filtro xpath che sennò 
        * fanno casino
        */        
        $totalXpath = "//c:Product[c:Visible[text()='false'] |
                                  c:Active[text()='true'] ]";
    }               

    // esegue xpath
    $nodesToRemove =  $xmlParser->xpath( $totalXpath ); 

    // rimove i nodi trovati dal DOM
    if( !empty( $nodesToRemove ) )
    {
      foreach( $nodesToRemove as $n)
      {     
        $domRef = dom_import_simplexml($n);          
        $domRef->parentNode->removeChild($domRef);
      }
    }
    
    // carica nodi rimanenti in un array
    $result['PRODUCTS'] = $xmlParser->xpath('//c:Product');
    return $result;
  }
  
  
  
  /**
   * Get all categories
   *   
   * @return Associative array with all merchant categories
   * 
   * $result['CATEGORIES'] = array with all categories
   *
   * Category is formed by: 
   * ["MerchantCategoryCode"]=> strinbg
   * ["CategoryName"]=> string  
   * ["Order"]=>  string 
   */
  public function get_merchant_categories()
  {
    $requestData['username'] = $this->username;
    $requestData['password'] = $this->password;      
    
    // crea xml
    $xml = $this->create_xml( 'GetMerchantCategories', 
                              $requestData );

    // effettua richiesta
    $result = $this->php_http_post_xml( $this->urlWS, $xml->asXML() );

    // dalla risposta in formato XML costruisce un array
    $result_semplified = $this->create_get_merchant_cat_result( $result );
    
    // l'array è il risultato
    return $result_semplified;
  }
  
  
  /**
   * Create the array result for GetMerchantCategories operation
   *
   * @param xmlstr  XML contenente la risposta del WS
   *
   * @return An array containing
   * ["MerchantCategoryCode"]=> string
   * ["CategoryName"]=> string  
   * ["Order"]=>  string 
   */
  private function create_get_merchant_cat_result( $xmlstr )
  {
    $result = array();
    $xmlParser = new \SimpleXMLElement( $xmlstr );
    
    // indispensabile per xpath, altrimenti i ns non vanno
    $xmlParser->registerXPathNamespace( 'c', 'http://tempuri.org/' );
    
      
    // Aggiungere tutti i nodi eventualmente da eliminare
    $nodesToRemove = "";//$xmlParser->xpath("//c:Dedication"); 
    
    // usa DOM per emilinare i nodi non necessari
    if( !empty ( $nodesToRemove ) )
    {
      foreach( $nodesToRemove as $n)
      {
        $domRef = dom_import_simplexml($n);
        $domRef->parentNode->removeChild($domRef);
      }
    }
    
    // cerca tutte le info riguardanti i voucher
    $result['CATEGORIES'] = $xmlParser->xpath('//c:MerchantCategory');     
    
    return $result;
  }
  
  
  /**
   * 
   */
  public function get_merchant_list_by_contractCode()
  {
    $requestData['username'] = $this->username;
    $requestData['password'] = $this->password;      
    $requestData['contractCode'] = $this->contractCode;

    
    // crea xml
    $xml = $this->create_xml( 'GetMerchantListByContractCode', 
                              $requestData );

    // effettua richiesta
    $result = $this->php_http_post_xml( $this->urlWS, $xml->asXML() );

    // dalla risposta in formato XML costruisce un array
    $result_semplified = $this->create_get_merchant_list_result( $result );
    
    // l'array è il risultato
    return $result_semplified;
  }
  
  
  /**
   * Create the array result for GetMerchantListByContractCode operation
   *
   * @param xmlstr  XML contenente la risposta del WS
   *
   * @return An array containing
   * ["MerchantCategoryCode"]=> string
   * ["CategoryName"]=> string  
   * ["Order"]=>  string 
   */
  private function create_get_merchant_list_result( $xmlstr )
  {
    $result = array();
    $xmlParser = new \SimpleXMLElement( $xmlstr );
    
    // indispensabile per xpath, altrimenti i ns non vanno
    $xmlParser->registerXPathNamespace( 'c', 'http://tempuri.org/' );
    
      
    // Aggiungere tutti i nodi eventualmente da eliminare
    $nodesToRemove;//$xmlParser->xpath("//c:Dedication"); 
    
    // usa DOM per emilinare i nodi non necessari
    if( !empty ( $nodesToRemove ) )
    {
      foreach( $nodesToRemove as $n)
      {
        $domRef = dom_import_simplexml($n);
        $domRef->parentNode->removeChild($domRef);
      }
    }
    
    // cerca tutte le info riguardanti i voucher
    $result['RETAILERS'] = $xmlParser->xpath('//c:Retailer');     
    
    return $result;
  }
  
  
  
  /**
   * Add the short operation response to array
   * The new field is OPERATIONRESPONSE and the value is OK or KO
   * 
   * @param $result_semplified: the array with the result structure
   *
   */
  private function add_operation_response( &$result_semplified, $ok_msg )
  {
    if( 'ok' == $result_semplified['OPERATIONRESULT'] || 
        $ok_msg  == substr( $result_semplified['OPERATIONRESULT'], 
                            0 ,
                            strlen( $ok_msg )))
    {
      $result_semplified['OPERATIONRESPONSE'] = 'OK';
    }
    else
    {
      $result_semplified['OPERATIONRESPONSE'] = 'KO';
    }
  }


  /**
   * Get category codes list by category type
   * 
   * 
   * @param $categoryType: 0 = ALL
   *                       1 = SHOPPING,
   *                       2 = BUONI BENZINA
   *                       3 = VIAGGI
   *                       4 = RIVISTE E TV
   * 
   * @return true if the amilon category matches with categoryType
   */
  private function get_cat_codes_by_type( $categoryType, &$merchCode, &$catCodes )
  {
    $merchCode = array();
    $catCodes = array();
    
    switch ( $categoryType )
    {
      // se 0 deve restituirle tutte
      case 0:        
      break;

      // 
      case 1:
          // ELETTRONICA E VIDEOGIOCHI
          array_push( $catCodes, 'e106c1f5-4cc0-43f6-a9c0-7f118cc35bf9' ); 
          
          // CIBO E RISTORANTI
          array_push( $catCodes, '75e0d88f-5045-4e60-880c-a9ccc4d1c225' );
          
          //GRANDI MAGAZZINI
          array_push( $catCodes, '00928908-8bb0-4791-963e-f2f83f0f2529' );
        
          // ABBIGLIMENTO
          array_push( $catCodes, '83a387b9-5de5-4257-9a41-18ab276e5b9b' );
          
          // SALUTE E BELLEZZA
          array_push( $catCodes, '176dea7e-7bdb-4f3e-bc47-3caf85f52a26' );
          
          // ARTICOLI SPORTIVI
          array_push( $catCodes, '3616c9a1-fc3a-41cc-9621-0f796fe35a22' );
          
          // OCCHIALI E OTTICA
          array_push( $catCodes, '981d7f2b-f5fd-4237-8ee8-8f9207ded0ec' );
          
          // MUSICA CINEMA E DIVERTIMENTO
          array_push( $catCodes, '514f81a0-6bd9-41da-a976-ff3080dd42b8' );
          
          // ECOMMERCE
          array_push( $catCodes, 'ddc0e789-dc8e-4c5f-8781-b46cde8ba5c2' );
          
          // PRODOTTI PER L INFANZIA
          array_push( $catCodes, '2cb10fb7-5fb0-4880-8e53-413658be1659' );
          
          // LIBRI E LIBRERIE
          array_push( $catCodes, '6bdf7beb-1885-4a2d-8d76-5bbf172021ea' );
          
          // BENEFICIENZA
          array_push( $catCodes, '431faa87-f73d-4cd8-9349-64309e9fb910' );
          
          // CASA, AUTO, ARREDAMENTO
          array_push( $catCodes, 'add9dbc3-e54d-4a4c-90c6-af50976bf746' );
          
          // BENNET
          array_push( $merchCode, '02e2f19f-ac1a-4413-9cec-abfbd27f2d75' );
          
          // COIN
          array_push( $merchCode, '22012cb9-5d68-4997-ad3a-e020184e0f94' );
          
          // TRONY
          array_push( $merchCode, '8fc2a7d0-3355-48b9-be70-0ff54c538f0a' );
      break;

      // BENZINA
      case 2:
          // IP
          array_push( $merchCode, '0be45cff-b0da-451b-bc46-d88347770ba0' );
      break;

      // Viaggi
      case 3:
        array_push( $catCodes, '350ca73d-4764-4a4f-b383-9343c1aac708' );
        array_push( $catCodes, '13c64b5b-7a55-4c14-9e1c-62d0b18d3330' );
      break;
      
      // Riviste
      case 4:
      break;
    }
  }


  /**
   * Create XML structure for the requests to Amilon WS
   * 
   * @param $operationName: name of the Amilon operation
   * @param $data: array with specific parameters for operation
   * 
   * @return XML structure
   * 
   */
  private function create_xml( $operationName, $data )
  {
    // Crea nodo radice
    $xml = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\"?> 
      <soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
                    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" 
                    xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">                                          
      </soap12:Envelope>");

    // crea nodo soap12:body
    $bodyXML = new \SimpleXMLElement( 
      "<soap12:Body xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">
      </soap12:Body>");    
        
    // Crea nodo specifico per operazione
    $operationXML = 
      new \SimpleXMLElement( "<" . $operationName . 
                            " xmlns=\"http://tempuri.org/\">
                            </" . $operationName . ">" );

    // Converte array in XML
    $this->array_to_xml( $data, $operationXML );

    // Mette insieme i pezzi per fare il file xml completo
    $this->sxml_append( $bodyXML, $operationXML );
    $this->sxml_append( $xml, $bodyXML );
    
    return $xml;
  }


  
  /**
   * Convert array to xml ( by stack overflow ...)
   * 
   * @param $array: Array to convert
   * @param $xml_user_info: xml  
   * 
   * @return 
   */
  function array_to_xml( $array, &$xml_user_info ) 
  {
    foreach( $array as $key => $value ) 
    {
      if( is_array( $value ) ) 
      {
          if( !is_numeric( $key ) )
          {
              $subnode = $xml_user_info->addChild( "$key" );
              $this->array_to_xml( $value, $subnode );
          }
          else
          {
              $subnode = $xml_user_info->addChild( "item$key" );
              $this->array_to_xml( $value, $subnode );
          }
      }
      else 
      {
          $xml_user_info->addChild( "$key", htmlspecialchars( "$value" ) );
      }
    }
  }



  /**
   * Create a POST request to URL with XML data
   * 
   * @param $url: url to contact
   * @param $post_data: xml data to send
   * 
   * @return HTTP response or exception if fails
   *
   * @DEPRECATED
   */
  function php_http_post_xml_old( $url, $post_data )
  {
    // Must be XML and require Content Length
    $requestHeaders = array('Content-type: text/xml', 
                            'Accept: */*',
                            sprintf('Content-Length: %d', 
                                      strlen( $post_data )));

    // Create context
    $context = stream_context_create(
                    array(
                        'http' => array(
                            'method'  => 'POST',
                            'header'  => implode("\r\n", $requestHeaders),
                            'content' => $post_data,
                        )
                    )
                );

    // get response
    $responseXML = file_get_contents( $url, false,  $context );
    
    // Se fallisce allora lancia eccezione
    if ( FALSE === $responseXML )
    {
      $this->mylog_message( __FUNCTION__ ,
                            "HTTP request failed",
                            "ERROR" );
      throw new RuntimeException( 'HTTP request failed...' );
    }
    else
    {
      $this->mylog_message( __FUNCTION__,
                            "HTTP REQUEST OK",
                            "INFO");
      return $responseXML;
    }
  }
  
  
  function php_http_post_xml($url, $post_data)
  {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                            'Content-Type: text/xml',
                                            'Content-Length: '. strlen($post_data),
                                            'Accept: */*'                                      
                                            ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    // curl_setopt($ch, CURLOPT_SSLVERSION,3);
  
    $output = curl_exec($ch);
    
    if( false == $output )
    {
      $this->mylog_message( __FUNCTION__, 
                      'HTTP request failed: ' . curl_error($ch),
                      'ERROR' );
                      
      throw new RuntimeException( 'HTTP request failed: ' . 
                                  (string)curl_error($ch) );
    }      
    curl_close($ch);   
    return $output;
  }


  /**
   * Merge two XML documents
   * 
   * @param $to: 
   * @param $from:
   */
  function sxml_append( \SimpleXMLElement $to, \SimpleXMLElement $from ) 
  {
    $toDom = dom_import_simplexml( $to );
    $fromDom = dom_import_simplexml( $from );
    $toDom->appendChild( $toDom->ownerDocument->importNode( $fromDom, true ) );
  }


  function mylog_message( $method, $message, $type )
  {
  //  dpm( $message, $method, $type );
  //  watchdog( $method, $message. log_info());
    /* echo "class_name:" .__CLASS__. "\n" .
          "function_name: ". $method . "\n".
          "msg:" . $message . "\n";*/
  }
  
  
}

