<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Logic that determines where to send the user
        if( $request->user()->hasRole('whisper') )
        {
            return view('home');
        }
        if( $request->user()->hasRole('admin') )
        {
            return view('home');
        }
        return view('home');
    }
}
