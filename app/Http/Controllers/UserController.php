<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//this to check hash user password
use Illuminate\Support\Facades\Hash;
use App\User;

/**
 * @todo creare una middlewere per controllare i permessi delle operazioni CRUD
 *  sui dati degli utentei
 * 
 * @todo aggiungere messaggi significativi per guidare l'utente nelle azioni e
 * negli esiti delle stesse, sia in caso di fallimento, avvertimento o successo
 * 
 */
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //controllo il ruolo dell'utente che tenta di accedere ai dati di tutti 
        //gli utenti
        $request->user()->authorizeRoles( ['admin'] );
      
        //get all users fields
        $users = new User;
        $users = $users->all();
        return view( 'mdl.users.index', [ 'users' => $users ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //controllo che l'utente corrente sia admin, altrimenti 401
        Auth::user()->authorizeRoles( ['admin'] );

        //visualizzo la vista per creare utenti
        return view('mdl.users.create'); // Only return the form
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //controllo che l'utente corrente sia admin, altrimenti 401
        Auth::user()->authorizeRoles( ['admin'] );
        Auth::user()->validate($request->all());

        User::create($request->all()); // Store the article on a rest wa

        return redirect()->back(); // or what ever you wan to do here
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * 
     * @todo inserire comunicazioni all'utente in caso non venga trovato nessun
     * utente con l'identificativo richiesto
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //get single user if exit
        $user = User::find( $id );
        //return view( 'mdl.users.profile', [ 'user' => $user ] );
        return view( 'mdl.users.show', [ 'user' => $user ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @todo mettere controllo su aggiornamento dati untente
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //$user = Auth::user();
        return view('mdl.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * 
     * @todo commentare i passaggi in cui definisco la validazione dei dati
     * @todo controllo su chi modifica i dati di chi
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(User $user)
    {
        //validations rules
        $rules = ['name' => 'required'];
        
        if( request('email') != $user->email )
        {
          $rules['email'] = 'required|email|unique:users';
        }
        if( request('password') )
        {
            $rules['password'] = 'required|min:3|confirmed';
        }
        
        $this->validate(request(), $rules);

        $user->name = request('name');
        $user->email = request('email');
        $user->password = bcrypt(request('password'));

        $user->save();
        
        return back();
    }

    /**
     * Show the form for confirm delete.
     */
    public function delete($id)
    {
        $user = User::findOrFail($id);
        return view('mdl.users.delete', [ 'user' => $user ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Auth::user()->authorizeRoles( ['admin'] );
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('user');
    }
}
