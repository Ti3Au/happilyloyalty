<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Validator;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * Rules to validate model fields
     * 
     * @var array
     */
    private $rules = array(
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6|confirmed'
    );


    /**
     * Test creazione fiend system
     */
    // Get list of who sent me a friend request
    public function friendToMe()
    {
        return $this->belongsToMany(get_class($this), 'friends', 'friend_id');
    }

    // Get the list of friends whom I invited
    public function meToFriend()
    {
        return $this->belongsToMany(get_class($this), 'friends', 'user_id', 'friend_id');
    }

    // Merge
    //get list of all friends
    public function friends()
    {
        return $this->friendToMe->merge($this->meToFriend);
    }


    /**
     * Validation fields via this->rules
     * use this for simple validations
     *
     * @var $data
     */
    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->errors();
            return false;
        }

        // validation pass
        return true;
    }


    public function errors()
    {
        return $this->errors;
    }

    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Provide a many-to-many relationship between User and Role
     */
    public function roles(){
        return $this->belongsToMany(Role::class);
    }


    /**
     * @param string|array $roles
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) 
        {
            return $this->hasAnyRole($roles) || 
                    abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) || 
                abort(401, 'This action is unauthorized.');
    }

    
    /**
     * Check multiple roles
     *
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     *
     * @param string $role
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }


    
}
