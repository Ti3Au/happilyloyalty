<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //add default roles
        $role_whisper = new Role();
        $role_whisper->name = 'whisper';
        $role_whisper->description = 'A basic user that use app';
        $role_whisper->save();

        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'An Admin user';
        $role_admin->save();
    }
}
