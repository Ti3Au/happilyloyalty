<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_whisper = Role::where('name', 'whisper')->first();
        $role_admin  = Role::where('name', 'admin')->first();
        
        $whisper = new User();
        $whisper->name = 'whisper Name';
        $whisper->email = 'im_was_the_barto@yahoo.it';
        $whisper->password = bcrypt('fallo');
        $whisper->save();
        $whisper->roles()->attach($role_whisper);

        $admin = new User();
        $admin->name = 'Kevin Bravo';
        $admin->email = 'kg.bravo92@gmail.com';
        $admin->password = bcrypt('fallo');
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
