//------------------------------------------------------------------------------
//  CONFIGURAZIONE DEL MODULO
//------------------------------------------------------------------------------
(function($) {

  device = {
  		xlarge: 1680,
  		large: 1280,
  		medium: 980,
  		tablet: 768,
  		phone: 640,
      ww : function(){
        return $(window).width();
      },
      isPhone: function(){
        return ( device.ww() <= device.phone ) ? 1 : 0 ;
      },
      isTablet: function(){
        return ( device.ww() <= device.tablet ) ? 1 : 0 ;
      }
  	}

    function phoneResize(){
      if( device.isPhone() ){
        // ------------------------------------------------
        //assegno agli elementi che hanno phone i
        // td.foofa-hide o td.foofa-hide
        console.log('is phone');
        $('.foofa-phone').addClass('foofa-hide');
        //console.log(elements_to_hide);
        //elements_to_hide.addClass('foofa-hide');
        // ------------------------------------------------
      }
    }
    function tabletResize(){
      if( device.isTablet() ){
        // ------------------------------------------------
        //assegno agli elementi che hanno phone i
        // td.foofa-hide o td.foofa-hide
        console.log('is tablet');
        $('.foofa-tablet').addClass('foofa-hide');
        //console.log(elements_to_hide);
        //elements_to_hide.addClass('foofa-hide');
        // ------------------------------------------------
      }
    }

$(function() {
  //------------------------------------------------------------------------------
  //  CORPO DEL MODULO
  //------------------------------------------------------------------------------

  phoneResize();
  tabletResize();

  // ---------------------------------------------
  //mappa degli headers delle colonne da nascondere
  //  a prescindere dal dispositivo
  hide_headers = $("th.foofa-hide").map(function() {
        return $(this).text();
    }
  ).get();

  // ---------------------------------------------------------------------------
  //gestisco la visualizzazione delle delle colonne nascoste a prescindere dal
  // dispositivo in uso dall'utente
  $('.fooFATable tr td').click( function(e){
    //this.next('');
    if( $(e.target).is('td,th') && $('td.foofa-hide').length ){

      var current_row = $(this).parent('tr');
      var campi_visibili = $(".fooFATable thead tr th:visible");

      if( current_row.next('.fooFA-rows-detail').length ){
        current_row.next('.fooFA-rows-detail').toggleClass('hide');
      }else{
        //creo il contenuto che l'utente vuole visualizzare in base alla
        //alla riga che ha cliccato

        //1) prendo i campi
        var hide_fields = current_row.find('td.foofa-hide').map(function() {
              return $(this).html();
            }).get();

        var myString = '<tr class="fooFA-rows-detail">'+
        '<td colspan="'+campi_visibili.length+'">';

        for( var i = 0; i < hide_fields.length; i++ ){
          myString += '<div class="fooFA-detail-row">'+
            '<div class="foo-header-show">'+hide_headers[i]+'</div>'+
            '<div class="foo-field-show">'+hide_fields[i]+'</div>'+
            '</div>';
        }
        myString += '</td></tr>';
        //stampo il valore dei campi nascosti in una riga successiva a
        //quella cliccata e quindi selezionata dall'utente

        //$('<tr class="fooFA-row-detail">'+myString+'</tr>')
        //  .insertAfter($(this).closest('tr'));

        current_row.after(myString);
      }
    }//fine if
  });

  // ---------------------------------------------------------------------------

  // ---------------------------------------------

//------------------------------------------------------------------------------
//  FINE CORPO DEL MODULO
//------------------------------------------------------------------------------
});// fine $(function() { ... } );

})(jQuery); //FINE SCRIPT
