@extends('mdl.main')                                  
@section( 'page_title', 'Profile' )

@section('content')

@if ( $user )

<div class="container mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">

        <div class="card-header">
          <a href="{{ action('UserController@edit', ['id' => Auth::user()->id]) }}">
              <i class="material-icons">create</i>
          </a>
          <p>
            Profilo
          </p>
          
        </div>
                
        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          <ul>
            <li>
                nome: {{ $user->name }}
            </li>
            <li>
                email: {{ $user->email }}
            </li>
          </ul>
        </div>

        <form action="{{ action('UserController@destroy', ['id' => $user->id]) }}" method="POST">
          {{ csrf_field() }}
          @method('DELETE')
          <button type="submit">Delete</button>
        </form>

      </div>
    </div>
  </div>
</div>


@else
    I don't have any records!
@endif

@endsection
