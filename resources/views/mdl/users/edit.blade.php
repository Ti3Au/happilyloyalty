@extends('mdl.main')                                  
@section( 'page_title', 'Edit profile' )

@section( 'assets_css' )

<link rel="stylesheet" href="{{ URL::asset('mdl/css/form.css') }}">

@endsection


@section('content')

<div class="container mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
  
    @if( Session::has( 'error_msg' ) )
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert" >x</button>
        {!! session( 'error_msg' ) !!}
    </div>
    @elseif( Session::has( 'success_msg' ) )
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert" >x</button>
        {!! session( 'success_msg' ) !!}
    </div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">

        <div class="card-header">
            <a href="{{ action('UserController@show', ['id' => Auth::user()->id]) }}">
                <i class="material-icons">account_circle</i>
            </a>
          <p>
            Profilo
          </p>
        </div>

        <div class="card-body">
          <form method="post" action="{{ action('UserController@update', ['id' => $user->id]) }}">
            {{ csrf_field() }}
            {{ method_field('patch') }}

            <div class="group">      
              <input type="text" name="name"  value="{{ $user->name }}" required/>
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>Name</label>
            </div>

            <div class="group">      
              <input type="email" name="email"  value="{{ $user->email }}" required/>
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>Email</label>
            </div>

            <div class="group">      
                <input type="password" name="password" />
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>New password</label>
            </div>

            <div class="group">      
                <input type="password" name="password_confirmation" />
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>Confirm new password</label>
            </div>
        
        
            
        
            
        
            <button type="submit">Send</button>
        </form>
        
        </div>
      </div>
    </div>
  </div>
</div>




@endsection
