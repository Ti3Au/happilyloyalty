@extends('mdl.main')                                  
@section( 'page_title', 'Users' )

@section( 'assets_css' )

<link rel="stylesheet" href="{{ URL::asset('mdl/css/table.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/fooFa.css') }}">

@endsection

@section( 'assets_js' )

<script src="{{ URL::asset('js/jquery-2-2-4-min.js') }}"></script>
<script src="{{ URL::asset('js/fooFa.js') }}"></script>

@endsection

@section('content')

@if ( $users )

<div class="container mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">

        <div class="card-header">
          <a href="{{ action('UserController@index') }}">
              <i class="material-icons">people</i>
          </a>
          <p>
            Users
          </p>
          
        </div>
                
        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          <div class="table-wrapper">
            <table class="alt fooFATable">
              <thead>
                <tr>
                  <!-- @todo trovare il modo corretto per estrapolare le chivi 
                    in automatico
                  -->
                  <th class="foofa-phone">id</th>
                  <th>name</th>
                  <th class="foofa-phone">email</th>
                  <th>actions</th>
                </tr>
              </thead>

              <tbody>
                @foreach ( $users as $user )
                  <tr class="{{ ($user->id%2 ? "odd" : "even") }}" >
                    <td class="foofa-phone">{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td class="foofa-phone">{{ $user->email }}</td>
                    <td>
                      <span>
                        <a href="{{ action('UserController@show', ['id' => $user->id]) }}">
                          <i title="show user details" class="material-icons">
                            visibility
                          </i>
                        </a>
                      </span>
                      &nbsp;
                      <span>
                        <a href="{{ action('UserController@edit', ['id' => $user->id]) }}">
                            <i title="edit user details" class="material-icons">
                              edit
                            </i>
                        </a>
                      </span>
                      &nbsp;
                      <span>
                        <a href="/user/{{$user->id}}/delete">
                            <i title="edit user details" class="material-icons">
                              delete_outline
                            </i>
                        </a>
                      </span>

                    </td>
                  </tr>
                @endforeach

              </tbody>

            </table>
          </div>

<?php


$lol = array( 'uno' => "val", "active" => true );

$asd = [ "lol" => $lol] ;

/*
[
  [ "nome" => "kevin", "ruolo" => "dip"], 
  [ "nome" => "mattia", "ruolo" => "dip"], 
  [ "nome" => "enrico", "ruolo" => "datore"] 
] ;
*/
$secondo_parametro = "dip";

$result = array_filter( $asd, function( $sbattiti ) use( $secondo_parametro )
  { 
    //var_dump( $sbattiti );
    $res = ( $secondo_parametro == $sbattiti["active"] );
    //$res = ( $secondo_parametro == $sbattiti["active"] );
    //return $res;
  }
);
//var_dump( $result );

          ?>

        </div>
      </div>
    </div>
  </div>
</div>


@else
    I don't have any records!
@endif

@endsection
