@extends('mdl.main')                                  
@section( 'page_title', 'Profile' )

@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb">
      <a href="{{ url('admin/dashboard') }}" title="Go to Home" class="tip-bottom">
        <i class="icon-home"></i> Home</a>
      <a href="#" class="current">Settings</a>
    </div>
    <h1>Admin settings</h1>
    @if( Session::has( 'error_msg' ) )
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert" >x</button>
        {!! session( 'error_msg' ) !!}
    </div>
    @elseif( Session::has( 'success_msg' ) )
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert" >x</button>
        {!! session( 'success_msg' ) !!}
    </div>
    @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
            <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Update password</h5>
          </div>
          <div class="widget-content nopadding">
              <form method="post" action="{{route('user.edit', $user)}}">
                  {{ csrf_field() }}
                  {{ method_field('patch') }}
              
                  <input type="text" name="name"  value="{{ $user->name }}" />
              
                  <input type="email" name="email"  value="{{ $user->email }}" />
              
                  <input type="password" name="password" />
              
                  <input type="password" name="password_confirmation" />
              
                  <button type="submit">Send</button>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection
