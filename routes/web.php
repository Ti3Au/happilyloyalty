<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//rotte da solo per utentei autenticati
Route::group(['middleware' => 'auth'], function()
{
    /*
    Route::get('user/{user}',  ['as' => 'user.edit', 
        'uses' => 'UserController@edit']);
    
    Route::patch('user/{user}/update',  ['as' => 'user.update', 
        'uses' => 'UserController@update']);
    */
    Route::resource('user', 'UserController');

    Route::get('/user/{user}/delete', 'UserController@delete');

    //forms to update admin data
    //Route::post('user/update-pwd', 'UserController@updatePassword');
});
